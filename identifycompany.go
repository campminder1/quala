package quala

import (
	"encoding/json"
	"time"
)

type IdentifyCompanyMessage struct {
	// Timestamp is the time the event happened. If left nil, then the time
	// will be set when the identify call was made
	Timestamp *time.Time

	// CompanyId identifies the company
	CompanyID string

	// CompanyTraits are the traits associated with this company
	CompanyTraits *CompanyTraits
}

// IdentifyCompany identifies a company
func (b *Beacon) IdentifyCompany(message IdentifyCompanyMessage) error {
	if len(message.CompanyID) <= 0 {
		return ErrCompanyIDRequired
	}

	b.enqueueChan <- message.toJSON()
	return nil
}

type identifyCompanyMessageJSON struct {
	Type          string                 `json:"type,omitempty"`
	Timestamp     string                 `json:"timestamp,omitempty"`
	CompanyID     string                 `json:"companyId,omitempty"`
	CompanyTraits map[string]interface{} `json:"companyTraits,omitempty"`
}

func (m IdentifyCompanyMessage) toJSON() identifyCompanyMessageJSON {

	im := identifyCompanyMessageJSON{
		Type:      identifyCompany,
		CompanyID: m.CompanyID,
	}

	// Set timestamp to now if none is set
	if m.Timestamp != nil {
		im.Timestamp = m.Timestamp.Format(timeFormat)
	} else {
		im.Timestamp = time.Now().Format(timeFormat)
	}

	// Flatten custom traits and company traits into one map
	if m.CompanyTraits != nil {
		companyTraits := map[string]interface{}{}
		b, _ := json.Marshal(m.CompanyTraits)
		json.Unmarshal(b, &companyTraits)

		if m.CompanyTraits.CustomProperties != nil {
			cp := m.CompanyTraits.CustomProperties
			for k, v := range cp {
				companyTraits[k] = v
			}
		}
		delete(companyTraits, "CustomProperties")
		im.CompanyTraits = companyTraits
	}
	return im
}
