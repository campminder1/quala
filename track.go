package quala

import (
	"encoding/json"
	"time"
)

var (
	Sum   AggregationType = "SUM"
	Avg   AggregationType = "AVG"
	First AggregationType = "FIRST"
	Last  AggregationType = "LAST"
	Count AggregationType = "COUNT"
)

// AggregationType is used to aggregate a signal's data
type AggregationType string

// TrackMessage contains all the properties of a signal. A track message can be
// associated with a user or a company, either UserID or CompanyID are required
type TrackMessage struct {
	// Timestamp is the time the event happened. If left nil, then the time
	// will be set when the identify call was made
	Timestamp *time.Time

	// Signal is the name and aggregation type of the signal
	Signal SignalInput

	// UserID represents a user in the Quala system
	UserID *string

	// CompanyID represents a customer in the Quala system
	CompanyID *string

	// Properties are the values that should be associated with the signal
	Properties *Properties
}

// SignalInput contains the name and aggregation type of the signal being
// tracked
type SignalInput struct {
	// Name is the name of the signal
	Name string `json:"name,omitempty"`

	// AggregationType is the type of aggregation used on the signal, defaults
	// to SUM
	AggregationType *AggregationType `json:"aggregationType,omitempty"`
}

// Properties describe the signal
type Properties struct {
	// Value is an arbitrary integer value that is contextualized by the signal
	// type
	Value *int `json:"value,omitempty"`

	// Currency is the revenue associated with a signal. This should be sent in
	// the ISO 4127 format. If this is not set, we assume the revenue to be in
	// US dollars
	Currency *string `json:"currency,omitempty"`

	// Revenue is the transaction amount tied to the signal
	Revenue *float64 `json:"revenue,omitempty"`

	// CustomProperties are any additional properties associated with the
	// signal
	CustomProperties *map[string]interface{} `json:",omitempty"`
}

// Track tracks a signal
func (b *Beacon) Track(message TrackMessage) error {
	if message.UserID == nil && message.CompanyID == nil {
		return ErrCompanyOrUserRequired
	}

	b.enqueueChan <- message.toJSON()
	return nil
}

type trackMessageJSON struct {
	Type       string                  `json:"type"`
	Timestamp  string                  `json:"timestamp"`
	Signal     SignalInput             `json:"signal,omitempty"`
	CompanyID  *string                 `json:"companyId,omitempty"`
	UserID     *string                 `json:"userId,omitempty"`
	Properties *map[string]interface{} `json:"properties,omitempty"`
}

func (m TrackMessage) toJSON() trackMessageJSON {

	im := trackMessageJSON{
		Type:      track,
		CompanyID: m.CompanyID,
		UserID:    m.UserID,
		Signal:    m.Signal,
	}

	if m.Timestamp != nil {
		im.Timestamp = m.Timestamp.Format(timeFormat)
	} else {
		im.Timestamp = time.Now().Format(timeFormat)
	}

	// Flatten custom traits and group traits into one map
	if m.Properties != nil {
		properties := map[string]interface{}{}
		b, _ := json.Marshal(m.Properties)
		json.Unmarshal(b, &properties)

		if m.Properties.CustomProperties != nil {
			cp := m.Properties.CustomProperties
			for k, v := range *cp {
				properties[k] = v
			}
		}

		delete(properties, "CustomProperties")
		im.Properties = &properties
	}
	return im
}
