package quala

import (
	"encoding/json"
	"net/http"
	"testing"
	"time"
)

func TestBeacon_Track(t *testing.T) {
	type fields struct {
		disable     bool
		writeKey    string
		host        string
		logFunction func(error)
		flushAt     int
		httpClient  *http.Client
		queue       []interface{}
	}
	type args struct {
		message TrackMessage
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name: "no user id or company id",
			fields: fields{
				flushAt: 100,
			},
			args: args{
				message: TrackMessage{},
			},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			b := &Beacon{
				disabled:    tt.fields.disable,
				writeKey:    tt.fields.writeKey,
				host:        tt.fields.host,
				logFunction: tt.fields.logFunction,
				flushAt:     tt.fields.flushAt,
				httpClient:  tt.fields.httpClient,
				queue:       tt.fields.queue,
			}
			if err := b.Track(tt.args.message); (err != nil) != tt.wantErr {
				t.Errorf("Beacon.Track() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestTrackMessage_toJSON(t *testing.T) {
	now := time.Now()
	userID := "123"
	companyID := "123"
	value := 1
	currency := "USD"
	revenue := 1.234
	type fields struct {
		Timestamp  *time.Time
		Signal     SignalInput
		UserID     *string
		CompanyID  *string
		Properties *Properties
	}
	tests := []struct {
		name   string
		fields fields
		want   trackMessageJSON
	}{
		{
			name: "flatten JSON",
			fields: fields{
				Timestamp: &now,
				Signal: SignalInput{
					Name:            "Signal",
					AggregationType: &Sum,
				},
				UserID:    &userID,
				CompanyID: &companyID,
				Properties: &Properties{
					Value:    &value,
					Currency: &currency,
					Revenue:  &revenue,
					CustomProperties: &map[string]interface{}{
						"key": "value",
					},
				},
			},
			want: trackMessageJSON{
				Type:      track,
				Timestamp: now.Format(time.RFC3339),
				Signal: SignalInput{
					Name:            "Signal",
					AggregationType: &Sum,
				},
				UserID:    &userID,
				CompanyID: &companyID,
				Properties: &map[string]interface{}{
					"value":    &value,
					"currency": &currency,
					"revenue":  &revenue,
					"key":      "value",
				},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			m := TrackMessage{
				Timestamp:  tt.fields.Timestamp,
				Signal:     tt.fields.Signal,
				UserID:     tt.fields.UserID,
				CompanyID:  tt.fields.CompanyID,
				Properties: tt.fields.Properties,
			}

			gotJSON, _ := json.Marshal(m.toJSON())
			wantJSON, _ := json.Marshal(tt.want)

			if string(gotJSON) != string(wantJSON) {
				t.Errorf("TrackMessage.toJSON() = %v, want %v", string(gotJSON), string(wantJSON))
			}
		})
	}
}
