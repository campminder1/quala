package quala

import (
	"net/http"
	"reflect"
	"testing"
	"time"
)

func TestBeacon_Milestone(t *testing.T) {
	type fields struct {
		disable     bool
		writeKey    string
		host        string
		logFunction func(error)
		flushAt     int
		httpClient  *http.Client
		queue       []interface{}
	}
	type args struct {
		message MilestoneMessage
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name:   "milestone is required",
			fields: fields{},
			args: args{
				message: MilestoneMessage{},
			},
			wantErr: true,
		},
		{
			name:   "companyid is required",
			fields: fields{},
			args: args{
				message: MilestoneMessage{
					Name: "this is a new milestone",
				},
			},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			b := &Beacon{
				disabled:    tt.fields.disable,
				writeKey:    tt.fields.writeKey,
				host:        tt.fields.host,
				logFunction: tt.fields.logFunction,
				flushAt:     tt.fields.flushAt,
				httpClient:  tt.fields.httpClient,
				queue:       tt.fields.queue,
			}
			if err := b.Milestone(tt.args.message); (err != nil) != tt.wantErr {
				t.Errorf("Beacon.Milestone() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestMilestoneMessage_toJSON(t *testing.T) {
	now := time.Now()
	mn := now.Format(time.RFC3339)
	description := "defeat scott"

	type fields struct {
		Timestamp     *time.Time
		CompanyID     string
		Name          string
		Description   *string
		MilestoneDate *time.Time
	}
	tests := []struct {
		name   string
		fields fields
		want   milestoneMessageJSON
	}{
		{
			name: "flatten JSON",
			fields: fields{
				Timestamp:     &now,
				CompanyID:     "123",
				Name:          "New Milestone",
				Description:   &description,
				MilestoneDate: &now,
			},
			want: milestoneMessageJSON{
				Type:          milestone,
				Timestamp:     now.Format(time.RFC3339),
				CompanyID:     "123",
				Name:          "New Milestone",
				Description:   &description,
				MilestoneDate: &mn,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			m := MilestoneMessage{
				Timestamp:     tt.fields.Timestamp,
				CompanyID:     tt.fields.CompanyID,
				Name:          tt.fields.Name,
				Description:   tt.fields.Description,
				MilestoneDate: tt.fields.MilestoneDate,
			}
			if got := m.toJSON(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("MilestoneMessage.toJSON() = %v, want %v", got, tt.want)
			}
		})
	}
}
