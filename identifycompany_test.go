package quala

import (
	"encoding/json"
	"net/http"
	"testing"
	"time"
)

func TestBeacon_IdentifyCompany(t *testing.T) {
	type fields struct {
		disable     bool
		writeKey    string
		host        string
		logFunction func(error)
		flushAt     int
		httpClient  *http.Client
		queue       []interface{}
	}
	type args struct {
		message IdentifyCompanyMessage
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name: "no companyId",
			fields: fields{
				disable: true,
			},
			args: args{
				message: IdentifyCompanyMessage{},
			},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			b := &Beacon{
				disabled:    tt.fields.disable,
				writeKey:    tt.fields.writeKey,
				host:        tt.fields.host,
				logFunction: tt.fields.logFunction,
				flushAt:     tt.fields.flushAt,
				httpClient:  tt.fields.httpClient,
				queue:       tt.fields.queue,
			}
			if err := b.IdentifyCompany(tt.args.message); (err != nil) != tt.wantErr {
				t.Errorf("Beacon.IdentifyCompany() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestIdentifyCompanyMessage_toJSON(t *testing.T) {
	now := time.Now()
	companyName := "7 evil exes corp"
	industry := "evil"
	plan := "to control the future of Ramona's love life"
	renewalDate := now
	accountCreatedDate := now
	ARR := 1.00

	type fields struct {
		Timestamp     *time.Time
		CompanyID     string
		CompanyTraits *CompanyTraits
	}
	tests := []struct {
		name   string
		fields fields
		want   identifyCompanyMessageJSON
	}{
		{
			name: "flatten JSON",
			fields: fields{
				Timestamp: &now,
				CompanyID: "123",
				CompanyTraits: &CompanyTraits{
					Name:               &companyName,
					Industry:           &industry,
					Plan:               &plan,
					RenewalDate:        &renewalDate,
					AccountCreatedDate: &accountCreatedDate,
					ARR:                &ARR,
					CustomProperties: map[string]interface{}{
						"key1": "value1",
					},
				},
			},
			want: identifyCompanyMessageJSON{
				Type:      identifyCompany,
				Timestamp: now.Format(time.RFC3339),
				CompanyID: "123",
				CompanyTraits: map[string]interface{}{
					"ARR":                1.00,
					"accountCreatedDate": now,
					"industry":           "evil",
					"key1":               "value1",
					"name":               "7 evil exes corp",
					"plan":               "to control the future of Ramona's love life",
					"renewalDate":        now,
				},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			m := IdentifyCompanyMessage{
				Timestamp:     tt.fields.Timestamp,
				CompanyID:     tt.fields.CompanyID,
				CompanyTraits: tt.fields.CompanyTraits,
			}

			gotJSON, _ := json.Marshal(m.toJSON())
			wantJSON, _ := json.Marshal(tt.want)

			if string(gotJSON) != string(wantJSON) {
				t.Errorf("IdentifyCompanyMessage.toJSON() = %v, want %v", string(gotJSON), string(wantJSON))
			}
		})
	}
}
