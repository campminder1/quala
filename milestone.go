package quala

import "time"

// MilestoneMessage contains the properties of a milestone
type MilestoneMessage struct {
	// Timestamp is the time the event happened. If left nil, then the time
	// will be set when the milestone call was made
	Timestamp *time.Time

	// CompanyID identifies the company associated with this milestone
	CompanyID string

	// Name of the milestone
	Name string

	// Description of the milestone
	Description *string

	// MilestoneDate is the date of the milestone
	MilestoneDate *time.Time
}

// Milestone tracks a milestone
func (b *Beacon) Milestone(message MilestoneMessage) error {
	if len(message.Name) <= 0 {
		return ErrMilestoneRequired
	}
	if len(message.CompanyID) <= 0 {
		return ErrCompanyIDRequired
	}
	b.enqueueChan <- message.toJSON()
	return nil
}

type milestoneMessageJSON struct {
	Type          string  `json:"type"`
	Timestamp     string  `json:"timestamp"`
	CompanyID     string  `json:"companyId"`
	Name          string  `json:"name"`
	Description   *string `json:"description,omitempty"`
	MilestoneDate *string `json:"milestoneDate,omitempty"`
}

func (m MilestoneMessage) toJSON() milestoneMessageJSON {

	im := milestoneMessageJSON{
		Type:        milestone,
		CompanyID:   m.CompanyID,
		Name:        m.Name,
		Description: m.Description,
	}

	if m.Timestamp != nil {
		im.Timestamp = m.Timestamp.Format(timeFormat)
	} else {
		im.Timestamp = time.Now().Format(timeFormat)
	}

	if m.MilestoneDate != nil {
		md := m.MilestoneDate.Format(time.RFC3339)
		im.MilestoneDate = &md
	}

	return im

}
