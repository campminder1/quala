package quala

import (
	"encoding/json"
	"time"
)

// IdentifyGroupMessage contains the properties of a group
type IdentifyGroupMessage struct {
	// Timestamp is the time the event happened. If left nil, then the time
	// will be set when the identify call was made
	Timestamp *time.Time

	// GroupId represents a group in Quala
	GroupID string

	// CompanyID represents a customer in the Quala system
	CompanyID *string

	// UserID represents a user in the Quala system
	UserID *string

	// Traits are the properties associated with the identified group
	Traits *GroupTraits
}

// GroupTraits are the traits of a group
type GroupTraits struct {

	// Name of Group
	Name string `json:"name"`

	// Type of Group i.e. team, project
	Type string `json:"type"`

	// CustomProperties are any additional properties associated
	// with the group
	CustomProperties map[string]interface{} `json:",omitempty"`
}

func (b *Beacon) IdentifyGroup(message IdentifyGroupMessage) {
	b.enqueueChan <- message.toJSON()
}

type identifyGroupMessageJSON struct {
	Type      string                 `json:"type"`
	Timestamp string                 `json:"timestamp"`
	GroupID   string                 `json:"groupId"`
	CompanyID *string                `json:"companyId,omitempty"`
	UserID    *string                `json:"userId,omitempty"`
	Traits    map[string]interface{} `json:"traits,omitempty"`
}

func (m IdentifyGroupMessage) toJSON() identifyGroupMessageJSON {

	im := identifyGroupMessageJSON{
		Type:      identifyGroup,
		CompanyID: m.CompanyID,
		GroupID:   m.GroupID,
		UserID:    m.UserID,
	}

	if m.Timestamp != nil {
		im.Timestamp = m.Timestamp.Format(time.RFC3339)
	} else {
		im.Timestamp = time.Now().Format(time.RFC3339)
	}

	// Flatten custom traits and group traits into one map
	if m.Traits != nil {
		groupTraits := map[string]interface{}{}
		b, _ := json.Marshal(m.Traits)
		json.Unmarshal(b, &groupTraits)

		if m.Traits.CustomProperties != nil {
			cp := m.Traits.CustomProperties
			for k, v := range cp {
				groupTraits[k] = v
			}
		}
		delete(groupTraits, "CustomProperties")
		im.Traits = groupTraits
	}
	return im
}
