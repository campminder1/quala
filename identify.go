package quala

import (
	"encoding/json"
	"time"
)

// IdentifyMessage contains the properties of a identify request
type IdentifyMessage struct {
	// Timestamp is the time the event happened. If left nil, then the time
	// will be set when the identify call was made
	Timestamp *time.Time

	// UserID is the id for the user being identified
	UserID *string

	// CompanyID is the id for the company that should the identified user
	// should be associated with
	CompanyID string

	// GroupID ties a user to a group
	GroupID *string

	// Traits associated with a user
	Traits *UserTraits

	// CompanyTraits associated with a company
	CompanyTraits *CompanyTraits
}

// UserTraits are the default traits of a user. Additional traits can be added
// as CustomProperties
type UserTraits struct {
	Avatar      *string `json:"avatar,omitempty"`
	Email       *string `json:"email,omitempty"`
	Description *string `json:"description,omitempty"`
	FirstName   *string `json:"firstName,omitempty"`
	LastName    *string `json:"lastName,omitempty"`
	Name        *string `json:"name,omitempty"`
	Phone       *string `json:"phone,omitempty"`
	Title       *string `json:"title,omitempty"`
	Username    *string `json:"username,omitempty"`
	Website     *string `json:"website,omitempty"`

	// IsContact will add this user to the companies contacts
	// section
	IsContact *bool `json:"isContact,omitempty"`

	// CustomProperties are any additional properties associated
	// with the user
	CustomProperties map[string]interface{} `json:",omitempty"`
}

// CompanyTraits the default traits of a company. Additional traits can
// be added as CustomProperties
type CompanyTraits struct {
	Name               *string    `json:"name,omitempty"`
	Industry           *string    `json:"industry,omitempty"`
	Plan               *string    `json:"plan,omitempty"`
	RenewalDate        *time.Time `json:"renewalDate,omitempty"`
	AccountCreatedDate *time.Time `json:"accountCreatedDate,omitempty"`

	// ARR (Annual Reoccurring Revenue), a decimal i.e. $1000.00
	ARR *float64 `json:"ARR,omitempty"`

	// CustomProperties are any additional properties associated
	// with the company
	CustomProperties map[string]interface{} `json:",omitempty"`
}

// Identify identifies a user or company
func (b *Beacon) Identify(message IdentifyMessage) error {
	if len(message.CompanyID) <= 0 {
		return ErrCompanyIDRequired
	}

	b.enqueueChan <- message.toJSON()
	return nil
}

type identifyMessageJSON struct {
	Type          string                 `json:"type"`
	Timestamp     string                 `json:"timestamp"`
	CompanyID     string                 `json:"companyId"`
	UserID        *string                `json:"userId,omitempty"`
	GroupID       *string                `json:"groupId,omitempty"`
	Traits        map[string]interface{} `json:"traits,omitempty"`
	CompanyTraits map[string]interface{} `json:"companyTraits,omitempty"`
}

func (m IdentifyMessage) toJSON() identifyMessageJSON {

	im := identifyMessageJSON{
		Type:      identify,
		UserID:    m.UserID,
		CompanyID: m.CompanyID,
		GroupID:   m.GroupID,
	}

	if m.Timestamp != nil {
		im.Timestamp = m.Timestamp.Format(timeFormat)
	} else {
		im.Timestamp = time.Now().Format(timeFormat)
	}

	// Flatten custom traits and user traits into one map
	if m.Traits != nil {
		traits := map[string]interface{}{}
		cp := m.Traits.CustomProperties

		b, _ := json.Marshal(m.Traits)
		json.Unmarshal(b, &traits)

		for k, v := range cp {
			traits[k] = v
		}

		delete(traits, "CustomProperties")
		im.Traits = traits
	}

	// Flatten custom traits and company traits into one map
	if m.CompanyTraits != nil {
		companyTraits := map[string]interface{}{}

		b, _ := json.Marshal(m.CompanyTraits)
		json.Unmarshal(b, &companyTraits)

		if m.CompanyTraits.CustomProperties != nil {
			cp := m.CompanyTraits.CustomProperties
			for k, v := range cp {
				companyTraits[k] = v
			}
		}
		delete(companyTraits, "CustomProperties")
		im.CompanyTraits = companyTraits
	}
	return im
}
