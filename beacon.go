package quala

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"time"
)

const (
	host          = "https://beacon.quala.io"
	batchEndpoint = "/v1/batch"
	timeFormat    = time.RFC3339

	identify        = "IDENTIFY"
	identifyCompany = "IDENTIFY_COMPANY"
	identifyGroup   = "IDENTIFY_GROUP"
	track           = "TRACK"
	screen          = "SCREEN"
	milestone       = "MILESTONE"
)

var (
	ErrInvalidWriteKey       = errors.New("you must pass your Quala project's writeKey")
	ErrCompanyOrUserRequired = errors.New("userId or companyId is required")
	ErrCompanyIDRequired     = errors.New("company id is required")
	ErrScreenNameRequired    = errors.New("screen name required")
	ErrMilestoneRequired     = errors.New("milestone is required")
)

// Beacon is a beacon instance
type Beacon struct {
	disabled       bool
	writeKey       string
	host           string
	flushAt        int
	logFunction    func(error)
	httpClient     *http.Client
	enqueueChan    chan interface{}
	doneChan       chan bool
	done           bool
	queue          []interface{}
	flushInterval  time.Duration
	autoFlushTimer *time.Timer
}

// Options configures a beacon instance
type Options struct {
	Host string

	// FlushAt is the number of messages that will accumulate before a
	// batch message is sent to Quala. Default to 20
	FlushAt int

	// Disabled  disable the beacon when set to false and defaults to true
	Disabled bool

	// Timeout configures the HTTP client's timeout duration
	Timeout time.Duration

	// FlushedInterval sets the time before flush automatically gets called
	// defaults to 10 seconds
	FlushedInterval time.Duration

	// LogFunction will be called whenever there is an error
	LogFunction func(error)
}

// NewBeacon returns a new beacon instance
func NewBeacon(writeKey string, options Options) (beacon *Beacon, err error) {
	// Create queue channel, buffer in case there are many enqueue calls at
	// once
	enqueueChan := make(chan interface{}, 100)
	doneChan := make(chan bool)

	if writeKey == "" {
		err = ErrInvalidWriteKey
		return
	}

	h := host
	if options.Host != "" {
		h = options.Host
	}

	client := &http.Client{
		Timeout: options.Timeout,
	}

	flushAt := 20
	if options.FlushAt > 0 {
		flushAt = options.FlushAt
	}

	logFunc := func(error) {}
	if options.LogFunction != nil {
		logFunc = options.LogFunction
	}

	flushInterval := time.Second * 20
	if options.FlushedInterval != 0 {
		flushInterval = options.FlushedInterval
	}

	beacon = &Beacon{
		disabled:      options.Disabled,
		enqueueChan:   enqueueChan,
		doneChan:      doneChan,
		httpClient:    client,
		host:          h,
		writeKey:      writeKey,
		flushAt:       flushAt,
		logFunction:   logFunc,
		flushInterval: flushInterval,
	}

	go beacon.iterate()

	return
}

// Shutdown will flush the rest of the queue message out of the buffer
func (b *Beacon) Shutdown() {

	// Cancel the auto flush timer to keep it from continuing to iterate
	b.autoFlushTimer = nil

	// Wait for current flush to complete
	b.doneChan <- true
	b.doneChan = nil

	// Drain queue
	for {
		select {
		case message := <-b.enqueueChan:
			b.queue = append(b.queue, message)
		default:
			// Close the enqueue channel to prevent new message
			// from coming in
			b.enqueueChan = nil

			// Set queue length the the remainder of the queue
			// (the request to Quala may fail due to size
			// constraints)
			b.flushAt = len(b.queue)

			// Final flush
			b.flush()
			return
		}
	}
}

func (b *Beacon) iterate() {

	defer func() {
		if !b.done {
			b.flush()
			go b.iterate()
		}
	}()

	b.autoFlushTimer = time.NewTimer(b.flushInterval)

	for {
		select {
		case message := <-b.enqueueChan:
			b.queue = append(b.queue, message)
			if len(b.queue) >= b.flushAt {
				return
			}
		case <-b.autoFlushTimer.C:
			return
		case <-b.doneChan:
			b.done = true
			return
		}
	}
}

type requestBody struct {
	Batch     []interface{} `json:"batch"`
	Timestamp string        `json:"timestamp"`
	SentAt    string        `json:"sentAt"`
}

func (b *Beacon) flush() {
	if len(b.queue) <= 0 {
		return
	}

	// Split batch from rest of queue
	batch, remainingQueue := separateFlushedMessaged(b.flushAt, b.queue)

	// Reassign the remaining queue
	b.queue = remainingQueue

	if b.disabled {
		// return if the beacon is disabled but continue to allow the
		// queue to be read
		return
	}

	now := time.Now().Format(timeFormat)
	requestBody := requestBody{
		Timestamp: now,
		SentAt:    now,
		Batch:     batch,
	}

	r, err := json.Marshal(requestBody)
	if err != nil {
		b.logFunction(err)
		return
	}

	bb := bytes.NewBuffer(r)

	req, err := http.NewRequest("POST", b.host+batchEndpoint, bb)
	if err != nil {
		b.logFunction(err)
		return
	}

	// Set up authorization header
	req.Header.Add("authorization", fmt.Sprintf("writeKey: %s", b.writeKey))

	// Set user agent
	req.Header.Add("user-agent", "beacon-golang/v1")

	// Set content type
	req.Header.Add("content-type", "application/json")

	retry(func() error {
		_, err = b.httpClient.Do(req)
		if err != nil {
			b.logFunction(err)
		}
		return err
	})
}

func separateFlushedMessaged(
	flushAt int,
	queue []interface{},
) (
	batch, remainder []interface{},
) {
	for i, m := range queue {
		if i < flushAt {
			batch = append(batch, m)
		} else {
			remainder = append(remainder, m)
		}
	}
	return
}

func retry(fn func() error) {
	for retryCount := 0; retryCount < 3; retryCount++ {
		err := fn()
		if err == nil {
			break
		}
		// Wait in the case of an error
		time.Sleep(time.Duration(250*2^retryCount) * time.Millisecond)
	}
}
