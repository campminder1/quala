# Quala Beacon

The Quala Beacon sends metrics data to the Quala system. The Beacon has the
ability to identify companies and users, create groups and milestones, and
track signals in Quala.

See the [documentation](https://docs.quala.io) for more info

## Usage

The quala beacon will regularly aggregate and send requests to Quala. Begin
this process by creating a new `Beacon` using `NewBeacon`. This will start
the iterator that will run on a regular interval or when enough messages enter
the queue.

It's recommended that `Shutdown` is called when the application instance
finishes. This will allow the beacon to flush the messages remaining in its
queue.

```go
package main

import (
    "gitlab.com/CampMinder/quala"
)

func main() {

    // Create a new beacon
    b, err := quala.NewBeacon("<quala write key>", quala.Options{})
    if err != nil {
        return
    }

    companyID := "1000"
    companyName := "My Company"

    // Identify a company
    b.Identify(quala.IdentifyMessage{
        CompanyID: companyID,
        CompanyTraits: &quala.CompanyTraits{
            Name: &companyName,
        },
    })

    newValue := 50

    // Track a new signal
    b.Track(quala.TrackMessage{
        CompanyID: &companyID,
        Signal: quala.SignalInput{
            Name: "New Data Signal",
        },
        Properties: &quala.Properties{
            Value: &newValue,
        },
    })

    b.Shutdown()
}

```
