package quala

import (
	"encoding/json"
	"testing"
	"time"
)

func TestIdentifyGroupMessage_toJSON(t *testing.T) {
	now := time.Now()
	userID := "123"
	companyID := "123"
	type fields struct {
		Timestamp *time.Time
		GroupID   string
		CompanyID *string
		UserID    *string
		Traits    *GroupTraits
	}
	tests := []struct {
		name   string
		fields fields
		want   identifyGroupMessageJSON
	}{
		{
			name: "flatten JSON",
			fields: fields{
				Timestamp: &now,
				GroupID:   "123",
				CompanyID: &companyID,
				UserID:    &userID,
				Traits: &GroupTraits{
					Name: "Group",
					Type: "Type",
					CustomProperties: map[string]interface{}{
						"key": "value",
					},
				},
			},
			want: identifyGroupMessageJSON{
				Type:      identifyGroup,
				Timestamp: now.Format(time.RFC3339),
				GroupID:   "123",
				CompanyID: &companyID,
				UserID:    &userID,
				Traits: map[string]interface{}{
					"name": "Group",
					"type": "Type",
					"key":  "value",
				},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			m := IdentifyGroupMessage{
				Timestamp: tt.fields.Timestamp,
				GroupID:   tt.fields.GroupID,
				CompanyID: tt.fields.CompanyID,
				UserID:    tt.fields.UserID,
				Traits:    tt.fields.Traits,
			}
			gotJSON, _ := json.Marshal(m.toJSON())
			wantJSON, _ := json.Marshal(tt.want)

			if string(gotJSON) != string(wantJSON) {
				t.Errorf("IdentifyGroupMessage.toJSON() = %v, want %v", string(gotJSON), string(wantJSON))
			}
		})
	}
}
