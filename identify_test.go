package quala

import (
	"encoding/json"
	"net/http"
	"testing"
	"time"
)

func TestBeacon_Identify(t *testing.T) {
	type fields struct {
		disable     bool
		writeKey    string
		host        string
		logFunction func(error)
		flushAt     int
		httpClient  *http.Client
		queue       []interface{}
	}
	type args struct {
		message IdentifyMessage
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name: "no companyId",
			fields: fields{
				disable: true,
			},
			args: args{
				message: IdentifyMessage{},
			},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			b := &Beacon{
				disabled:    tt.fields.disable,
				writeKey:    tt.fields.writeKey,
				host:        tt.fields.host,
				logFunction: tt.fields.logFunction,
				flushAt:     tt.fields.flushAt,
				httpClient:  tt.fields.httpClient,
				queue:       tt.fields.queue,
			}
			if err := b.Identify(tt.args.message); (err != nil) != tt.wantErr {
				t.Errorf("Beacon.Identify() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestIdentifyMessage_toJSON(t *testing.T) {
	now := time.Now()
	userID := "123"
	avatar := "avatar link"
	description := "the hero of the story"
	email := "user@company.com"
	firstName := "scott"
	lastName := "pilgrim"
	name := "scott pilgrim"
	phone := "1-800-555-1234"
	title := "figher"
	username := "username"
	website := "scottpilgrim.com"
	isContact := true

	companyName := "7 evil exes corp"
	industry := "evil"
	plan := "to control the future of Ramona's love life"
	renewalDate := now
	accountCreatedDate := now
	ARR := 1.00

	type fields struct {
		Timestamp     *time.Time
		UserID        *string
		CompanyID     string
		GroupID       *string
		Traits        *UserTraits
		CompanyTraits *CompanyTraits
	}
	tests := []struct {
		name   string
		fields fields
		want   identifyMessageJSON
	}{
		{
			name: "flatten JSON",
			fields: fields{
				Timestamp: &now,
				UserID:    &userID,
				CompanyID: "123",
				Traits: &UserTraits{
					Avatar:      &avatar,
					Description: &description,
					Email:       &email,
					FirstName:   &firstName,
					LastName:    &lastName,
					Name:        &name,
					Phone:       &phone,
					Title:       &title,
					Username:    &username,
					Website:     &website,
					IsContact:   &isContact,

					CustomProperties: map[string]interface{}{
						"key1": "value1",
					},
				},
				CompanyTraits: &CompanyTraits{
					Name:               &companyName,
					Industry:           &industry,
					Plan:               &plan,
					RenewalDate:        &renewalDate,
					AccountCreatedDate: &accountCreatedDate,
					ARR:                &ARR,
					CustomProperties: map[string]interface{}{
						"key1": "value1",
					},
				},
			},
			want: identifyMessageJSON{
				Type:      identify,
				Timestamp: now.Format(time.RFC3339),
				UserID:    &userID,
				CompanyID: "123",
				Traits: map[string]interface{}{
					"avatar":      "avatar link",
					"description": "the hero of the story",
					"email":       "user@company.com",
					"firstName":   "scott",
					"key1":        "value1",
					"lastName":    "pilgrim",
					"name":        "scott pilgrim",
					"phone":       "1-800-555-1234",
					"title":       "figher",
					"username":    "username",
					"website":     "scottpilgrim.com",
					"isContact":   true,
				},
				CompanyTraits: map[string]interface{}{
					"ARR":                1.00,
					"accountCreatedDate": now,
					"industry":           "evil",
					"key1":               "value1",
					"name":               "7 evil exes corp",
					"plan":               "to control the future of Ramona's love life",
					"renewalDate":        now,
				},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			m := IdentifyMessage{
				Timestamp:     tt.fields.Timestamp,
				UserID:        tt.fields.UserID,
				CompanyID:     tt.fields.CompanyID,
				GroupID:       tt.fields.GroupID,
				Traits:        tt.fields.Traits,
				CompanyTraits: tt.fields.CompanyTraits,
			}

			gotJSON, _ := json.Marshal(m.toJSON())
			wantJSON, _ := json.Marshal(tt.want)

			if string(gotJSON) != string(wantJSON) {
				t.Errorf("IdentifyMessage.toJSON() = %v, want %v", string(gotJSON), string(wantJSON))
			}
		})
	}
}
