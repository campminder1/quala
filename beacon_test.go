package quala

import (
	"reflect"
	"testing"
)

func TestNewBeacon(t *testing.T) {
	type args struct {
		writeKey string
		options  Options
	}
	tests := []struct {
		name       string
		args       args
		wantBeacon *Beacon
		wantErr    bool
	}{
		{
			name: "missing write key",
			args: args{
				writeKey: "",
				options:  Options{},
			},
			wantBeacon: nil,
			wantErr:    true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			gotBeacon, err := NewBeacon(tt.args.writeKey, tt.args.options)
			if (err != nil) != tt.wantErr {
				t.Errorf("NewBeacon() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(gotBeacon, tt.wantBeacon) {
				t.Errorf("NewBeacon() = %v, want %v", gotBeacon, tt.wantBeacon)
			}
		})
	}
}

func Test_separateFlushedMessaged(t *testing.T) {
	type args struct {
		flushAt int
		queue   []interface{}
	}
	tests := []struct {
		name          string
		args          args
		wantBatch     []interface{}
		wantRemainder []interface{}
	}{
		{
			name: "single item",
			args: args{
				flushAt: 1,
				queue: []interface{}{
					"test",
				},
			},
			wantBatch: []interface{}{
				"test",
			},
			wantRemainder: []interface{}{},
		},
		{
			name: "two items",
			args: args{
				flushAt: 1,
				queue: []interface{}{
					"test",
					"test2",
				},
			},
			wantBatch: []interface{}{
				"test",
			},
			wantRemainder: []interface{}{
				"test2",
			},
		},
		{
			name: "two flushed",
			args: args{
				flushAt: 2,
				queue: []interface{}{
					"test",
					"test2",
					"test3",
				},
			},
			wantBatch: []interface{}{
				"test",
				"test2",
			},
			wantRemainder: []interface{}{
				"test3",
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			gotBatch, gotRemainder := separateFlushedMessaged(tt.args.flushAt, tt.args.queue)
			if len(gotBatch) != len(tt.wantBatch) {
				t.Errorf("separateFlushedMessaged() gotBatch = %v, want %v", gotBatch, tt.wantBatch)
			}
			if len(gotRemainder) != len(tt.wantRemainder) {
				t.Errorf("separateFlushedMessaged() gotRemainder = %v, want %v", gotRemainder, tt.wantRemainder)
			}
		})
	}
}
